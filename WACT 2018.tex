\documentclass[xcolor=svgnames]{beamer}
\usepackage{beamerleanprogress}

\usepackage[utf8]    {inputenc}
\usepackage[T1]      {fontenc}
\usepackage[english] {babel}

\usepackage{amsmath,amssymb,amsfonts,graphicx}
\usepackage{MnSymbol}
\usepackage{commons}
\input{macros}

\usepackage{tikz}
\usetikzlibrary{shapes,arrows}
\usetikzlibrary{matrix}
\usetikzlibrary{calc}
\usetikzlibrary{decorations.pathmorphing}

\usepackage[linesnumbered,vlined,boxed,ruled]{algorithm2e}

\newcommand{\tmark}[1]{\tikz[remember picture, overlay] \node(#1){};}

\renewcommand{\phi}{\varphi}

\title{Constructing Faithful Maps\\over\\Arbitrary Fields}
\author
  {Prerona Chatterjee\\ \vspace{.5em} \tiny{joint work with}\\ \small{Ramprasad Saptharishi}}

\date{March 8, 2018}
\institute{WACT 2018}

\begin{document}

\tikzset{%
	block/.style    = {draw, thick, rectangle, minimum height = 3em,
		minimum width = 3em},
	sum/.style      = {draw, circle, node distance = 2cm}, % Adder
	input/.style    = {coordinate}, % Input
	output/.style   = {coordinate} % Output
}
\newcommand{\suma}{\Large$+$}
\newcommand{\proda}{\Large$\times$}

\maketitle

\section{Preliminaries}

\begin{frame}{Algebraic Independence}
\begin{block}{Definition: Algebraic Independence}
	A given set of polynomials $\set{\fm} \subseteq \F[\x]$ is said to be algebraically dependent if there is a non-zero polynomial combination of these that is zero.\\ \\    
	Otherwise, they are said to be algebraically independent.
\end{block}\pause

\begin{center}
	For a set of polynomials $\set{\fm}$, the family of all algebraically independent subsets form a matroid. \pause
\end{center} \[\text{Thus, } \algrank(\fm) \text{ is well defined.}\]\pause
\vspace{.2em}
\begin{center}
	\textbf{Question}: Can we test algebraic independence efficiently?
\end{center}
\end{frame}

\begin{frame}{Checking Algebraic Independence}
	\begin{block}{Working with Annihilating Polynomials [Kay09, GSS18]}
		Checking whether the constant term of all the annihilating polynomials is zero is $\NP$-hard.
	\end{block} \pause
	\textbf{Over Characteristic Zero fields}:\\ For $\fm \in \F[\x]$ and $\vecf = (\fm)$, 
	\[\J_\vecx(\vecf) = 
	\begin{bmatrix} %change order
	\partial_{x_1}(f_1) & \partial_{x_2}(f_1) & \ldots & \partial_{x_n}(f_1)\\
	\partial_{x_1}(f_2) & \partial_{x_2}(f_2) & \ldots & \partial_{x_n}(f_2)\\
	\vdots & \vdots & \ddots & \vdots\\
	\partial_{x_1}(f_m) & \partial_{x_2}(f_m) & \ldots & \partial_{x_n}(f_m)\\
	\end{bmatrix}\] \pause
    \begin{block}{The Jacobian Criterion [Jac41]}
    If $\F$ has characteristic zero, $\set{\fm}$ is algebraically independent if and only if its Jacobian matrix is full rank.
    \end{block}
\end{frame}

\begin{frame}{Rank Preserving Maps}
	\textbf{Basis in Linear Algebra}: Given a set of vectors $\set{v_1, v_2, \ldots, v_m}$ with linear rank $k$, there is a basis of size $k$.\\ \pause
	\vspace{1em}
	\begin{block}{Definition: Faithful Maps}
		Given a set of polynomials $\set{\fm}$ with algebraic rank $k$, a map 
		\vspace{-1em}
		\[\phi: \set{\x} \to \F[\yk]\] is said to be a faithful map if the algebraic rank of $\set{f_1(\phi), f_2(\phi), \ldots, f_m(\phi)}$ is also $k$.
	\end{block}\pause
	\vspace{.5em}
	\begin{center}
		\textbf{Question}: Can we construct faithful maps efficiently?\\\pause
		\vspace{.5em}
		\textbf{Bonus}: Helps in polynomial identity testing.
	\end{center}
\end{frame}

\begin{frame}{Faithful Maps and Poly. Identity Testing [BMS11, ASSS12]}
\begin{columns}
	\begin{column}{.3\textwidth}
		\begin{tikzpicture}[thick]
		\uncover<1->{\draw (0,0) circle (.3);
			\draw[->] (0,.3) -- (0,1);
			\node at (.5,.8) {$\ckt$};}
		\uncover<1-2>{
			\draw (-.2,-.25) -- (-1.6,-3.2) -- (1.6,-3.2) -- (.2,-.25);
			\draw (-.3,-1.5) circle (.3);
			\node at (-.3,-1.5) {$\times$};
			\draw (.5,-2.2) circle (.3);
			\node at (.5,-2.2) {$+$};
			\draw (-1.3,-3.4) -- (-1.3,-3);
			\draw (-.5,-3.4) -- (-.5, -3);
			\draw (1.3,-3.4) -- (1.3,-3);
			\draw (-1.3,-3.6) circle (.2);
			\draw (-.5,-3.6) circle (.2);
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};
			\draw (1.3,-3.6) circle (.2);}
		\uncover<1,3>{
			\node at (-1.3,-3.6) {\tiny{$x_1$}};
			\node at (-.5,-3.6) {\tiny{$x_2$}};
			\node at (1.3,-3.6) {\tiny{$x_n$}};
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};}
		\uncover<3->{
			\draw (-.2,-.25) -- (-1.5,-2) -- (1.5,-2) -- (.2,-.25);
			\draw (-1.1,-1.9) -- (-1.1,-2.2) -- (-1.6,-2.8) -- (-.6,-2.8) -- (-1.1,-2.2);
			\node at (-1.1,-2.55) {$f_1$};
			\node at (-.2,-2.55) {$\cdots$};
			\node at (.2,-2.55) {$\cdots$};
			\node at (1.1,-2.55) {$f_m$};
			\draw (1.1,-1.9) -- (1.1,-2.2) -- (1.6,-2.8) -- (.6,-2.8) -- (1.1,-2.2);
			\draw (-1.4,-2.7) -- (-1.4,-3);
			\draw (-1.3,-2.7) -- (-1.3,-3);
			\node at (-1.05,-3) {$\cdots$};
			\draw (-.8,-2.7) -- (-.8,-3);
			\draw (1.4,-2.7) -- (1.4,-3);
			\draw (1.3,-2.7) -- (1.3,-3);
			\node at (1.05,-3) {$\cdots$};
			\draw (.8,-2.7) -- (.8,-3);
			\draw (-1.3,-3.6) circle (.2);
			\draw (-.5,-3.6) circle (.2);
			\node at (.1,-3.6) {$\cdots$};
			\node at (.6,-3.6) {$\cdots$};
			\draw (1.3,-3.6) circle (.2);
			\draw (-1.3,-3.4) -- (-1.3,-3.2);
			\draw (-.5,-3.4) -- (-.5, -3.2);
			\draw (1.3,-3.4) -- (1.3,-3.2);}
		\uncover<2,4->{
			\node at (-1.3,-3.6) {\tiny{$\phi_1$}};
			\node at (-.5,-3.6) {\tiny{$\phi_2$}};
			\node at (1.3,-3.6) {\tiny{$\phi_n$}};
			\draw (-1.4,-3.75) -- (-1.6,-4.1) -- (-1,-4.1) -- (-1.2,-3.75);
			\draw (-.6,-3.75) -- (-.8,-4.1) -- (-.2,-4.1) -- (-.4,-3.75);
			\draw (1.2,-3.75) -- (1,-4.1) -- (1.6,-4.1) -- (1.4,-3.75);
			\draw (-1.5,-4.05) -- (-1.5,-4.2);
			\draw (-1.4,-4.05) -- (-1.4,-4.2);
			\draw (-1.1,-4.05) -- (-1.1,-4.2);
			\draw (-.7,-4.05) -- (-.7,-4.2);
			\draw (-.6,-4.05) -- (-.6,-4.2);
			\draw (-.3,-4.05) -- (-.3,-4.2);
			\draw (1.1,-4.05) -- (1.1,-4.2);
			\draw (1.2,-4.05) -- (1.2,-4.2);
			\draw (1.5,-4.05) -- (1.5,-4.2);
			\node at (-1.3,-5) {$y_1$};
			\node at (-.7,-5) {$y_2$};
			\node at (-.05,-5) {$\cdots$};
			\node at (.5,-5) {$\cdots$};
			\node at (1.3,-5) {$y_k$};
			\draw (-1.3,-4.4) -- (-1.3,-4.7);
			\draw (-.7,-4.4) -- (-.7, -4.7);
			\draw (1.3,-4.4) -- (1.3,-4.7);}
		\end{tikzpicture}
	\end{column}
	\begin{column}{.575\textwidth}
		\uncover<1->{Check whether $\ckt$ computes the zero polynomial or 	 	
			not.\\ \vspace{1em}}
		\uncover<2->{$\PIT \equiv$ Variable substitution preserving non-zeroness\\ \vspace{1em}}
		\uncover<3->{$\ckt \equiv \ckt(\fm)$: algebraic rank $k$\\ \vspace{1em}}
		\uncover<4->{$\phi: \set{\x} \to \F[\yk]$ 
		\begin{center}
			is a faithful map.
		\end{center}}
		\uncover<5>{
			\begin{tikzpicture}[thick]
			\draw (0,-2) rectangle (6,0);
			\node at (3,-.5) {$\ckt(\fm) \neq 0 \text{ if and only if}$};	\node at (3,-1.5) {$\inparen{\ckt(f_1(\phi), f_2(\phi), \ldots f_m(\phi))} \neq 0.$};
			\end{tikzpicture}}
	\end{column}
\end{columns}
\end{frame}

\begin{frame}{A Quick Survey}
	[Jac41]: Gave a criterion for checking Algebraic Independence over Characteristic zero fields.\\ \pause
	\vspace{1.5em}
	[BMS13]: Introduced the problem and the concept of faithful maps.\\
	Applied faithful maps to solve PIT when $\fm$ are sparse.\\ \pause
	\vspace{1.5em}
	[ASSS16]: Extended these techniques to a variety of other models.\\ \pause
	\vspace{1.5em}
	[PSS18]: Gave a criterion for checking Algebraic Independence over arbitrary fields.\\ \pause
	\vspace{1.5em}
	\textbf{This work}: Construct Faithful Maps over arbitrary fields and extend results in [ASSS16] to other fields.
\end{frame}

\section{Characteristic Zero Fields}

\begin{frame}{Constructing Faithful Maps over Characteristic Zero Fields}
	\only<1-2>{\begin{center}
			\textbf{Fact}: A random affine transformation is a faithful map\\ \vspace{3em}
			\end{center}}
	\vspace{-2em}
	\[\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i\] \pause
	\only<2>{\begin{center}
			\vspace{3em}
			\textbf{Question}: Can we construct faithful maps deterministically?
			\end{center}}
	\vspace{.5em}
	\only<3>{\[\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\end{bmatrix}\]}
	\only<4->{\[\begin{bmatrix}
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \J_\vecy(\vecf(\phi)) & \text{ }\\ 
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \phi(\J_\vecx(\vecf)) & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	\times 
	\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & M_\phi & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	\end{bmatrix}\]}
	\vspace{.5em}
	\only<5->{\textbf{What we need:} $\phi$ such that
	\begin{enumerate}
		\item $\rank(\J_\vecx(\vecf)) = \rank(\phi(\J_\vecx(\vecf)))$
		\only<6->{: $a_i$s are responsible for this}
		\uncover<7->{\item $M_\phi$ preserves rank}
	\end{enumerate}}
\end{frame}

\begin{frame}{A Rank Preserving Matrix and a Faithful Map [BMS13]}
	\begin{columns}
		\begin{column}{.5\textwidth}
			\uncover<1->{\begin{center}
				\vspace{-3em}
				\[\phi : x_i = \sum_{j=1}^{k}s_{ij}y_j + a_i\]
				Chain Rule $\Rightarrow M_\phi[i,j] = s_{ij}$ 
			\end{center}\pause}
			\uncover<2->{\begin{center}
				For every $m \times n$ matrix $A$, $\rank(A) = \rank(AM_\phi)$.
			\end{center} \pause}
			\uncover<3->{Family of matrices or one matrix parameterised by $s$: $\set{M_{\phi(s)}}_{s \in \mathcal{F}}$}
			\uncover<5->{\[\phi : x_i = \sum_{j=1}^{k}s^{ij}y_j + a_i \text{ will work.}\]}
		\end{column}
		\begin{column}{.5\textwidth}
			\uncover<4->{[GR05]: Vandermonde type matrices preserve rank.
				\vspace{.5em}
				\[\begin{bmatrix}
				s & s^2 & \ldots & s^k\\
				s^2 & s^4 & \ldots & s^{2k}\\
				\vdots & \vdots & \text{ } & \vdots\\
				\vdots & \vdots & \ddots & \vdots\\
				\vdots & \vdots & \ddots & \vdots\\
				\vdots & \vdots & \text{ } & \vdots\\
				s^n & s^{2n} & \ldots & s^{kn}
				\end{bmatrix}\]}
		\end{column}
	\end{columns}
\end{frame}

\section{The PSS Criterion}

\begin{frame}{What goes wrong over arbitrary fields?}
	Jacobian Matrix has partial derivatives as entries \pause
	- Entries can start becoming zero \pause 
	: Not the only case.\\ \pause
	\vspace{1em}
	\begin{center}
		$f_1 = xy^{p-1}$, $f_2 = x^{p-1}y$ : Algebraically Independent over $\F_p$.\pause
	\end{center}
	\[\J_{x,y} = \begin{bmatrix}
		y^{p-1} & (p-1)xy^{p-2}\\
		(p-1)x^{p-2}y & x^{p-1}
		\end{bmatrix}\]
		\[\det(\J_{x,y}) = (xy)^{p-1} - (p^2 - 2p + 1) (xy)^{p-1} = 0 \text{ over }\F_p.\] \pause
		
		\textbf{Characteristic Zero}: $\J$ has full rank $\Leftarrow$ $\J$ has an inverse\\ \pause
		\vspace{1em}
		\textbf{Finite Characteristic}: Entries in "inverse" have denominators that are partial derivatives of some annihilators, which can become zero.
\end{frame}

\begin{frame}{Looking Further in the Taylor Expansion [PSS18]}
	For any $f \in \F[\x]$ and $\vecz \in \F^n$,
	\[f(\vecx + \vecz) - f(\vecz) = \underbrace{x_1 \cdot \partial_{x_1}f + \cdots + x_n \cdot \partial_{x_n}f}_{\text{Jacobian}} + \text{ higher order terms}\]\pause
	
	[PSS18]: Look at Taylor expansions up to the "inseparable degree". \pause
	
	\vspace{1em}
	\begin{columns}
		\begin{column}{.55\textwidth}
			\begin{block}{Definition: A new Operator}
				For any $f \in \F[\x]$,
				\[\Ech_t(f) = \deg^{\leq t} \inparen{f(\vecx + \vecz) - f(\vecz)}\]
			\end{block}
		\end{column}\pause
		\begin{column}{.45\textwidth}
			\[\hat{\Ech}(\vecf) = \begin{bmatrix}
			& \ldots & \Ech_t(f_1) & \ldots & \\
			& \ldots & \Ech_t(f_2) & \ldots & \\
			& & \vdots\\
			& \ldots & \Ech_t(f_k) & \ldots &
			\end{bmatrix}.\]
		\end{column}
	\end{columns}
\end{frame}

\begin{frame}{The [PSS18] Criterion}
	A given set of polynomials $\set{\fk} \in \F[\x]$ is algebraically independent if and only if for a random $\vecz \in \F^n$, $\set{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}$ are linearly independent in  \[\frac{\F(\vecz)[\x]}{\I_t}\] where $t$ is the inseparable degree of $\set{\fk}$ and 
	\[
		\I_t = \genset{\Ech_t(f_1), \Ech_t(f_2), \ldots, \Ech_t(f_k)}^{\geq 2}_{\F(\vecz)} \bmod{\genset{\vecx}^{t+1}} \subseteq \F(\vecz)[\vecx].
	\]
\end{frame}

\begin{frame}{Alternate Statement for the [PSS18] Criterion}

	$\set{\fk}$ is algebraically independent if and only if for every $\inparen{v_1, v_2, \ldots, v_k}$ with $v_i \text{s in } \I_t$,
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix} \text{ has full rank over } \F(\vecz).\]
\end{frame}

\begin{frame}{The Goal}
	\textbf{What we know}:
	\[\Ech(\vecf, \vecv) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1) + v_1 & \ldots & \\
	& \ldots & \Ech_t(f_2) + v_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k) + v_k & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $v_1, v_2, \ldots, v_k \in \I_t$.
	\end{center}\pause

	\textbf{What we want to show}:
	\[\Ech(\vecf(\phi), \vecu) = \begin{bmatrix}
	& \ldots & \Ech_t(f_1(\phi)) + u_1 & \ldots & \\
	& \ldots & \Ech_t(f_2(\phi)) + u_2 & \ldots & \\
	& & \vdots\\
	& \ldots & \Ech_t(f_k(\phi)) + u_k & \ldots &
	\end{bmatrix}\] 
	\begin{center}
		has full rank for every $u_1, u_2, \ldots, u_k \in \I_t(\phi)$
	\end{center}	
\end{frame}

\section{Faithful Maps over Arbitrary Fields}

\begin{frame}{Constructing Faithful Maps over Arbitrary Fields}
	\[\phi : x_i \to \sum_{j=1}^{k}s_{ij}y_j + a_iy_0 \text{ and } z_i \to \sum_{j=1}^{k}s_{ij}w_j + a_iw_0\] \pause
	
	\vspace{1.5em}
	\underline{\textbf{Sufficient Properties}}
	\begin{enumerate}
		\vspace{.5em}
		\item For every $\vecu$, there is a $\vecv$ for which $\Ech(\vecf(\phi), \vecu) = \Ech(\vecf(\phi), \vecv(\phi))$\\ \pause
		\vspace{.5em}
		\item $\Ech(\vecf(\phi), \vecv(\phi)) = \phi(\Ech(\vecf,\vecv)) \times M_\phi$: Chain Rule \pause
		\vspace{.5em}
		\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi(\Ech(\vecf, \vecv)))$: $a_i$s are responsible for this \pause
		\vspace{.5em}
		\item $M_\phi$ preserves rank
	\end{enumerate}
\end{frame}

\begin{frame}{The Matrix Decomposition}	
	\[\hspace{-2.25em}
		\begin{bmatrix}
			\text{ } & \text{ } & \text{ } \\
			\text{ } & \Ech(\vecf(\phi), \vecv(\phi)) & \text{ } \\
			\text{ } & \text{ } & \text{ } 
		\end{bmatrix}
		=
		\overbrace{\begin{bmatrix}
			& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
			& \text{ } & \text{ } & \phi(\Ech(\vecf,\vecv)) & \text{ } & \text{ } &\\
			& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}}^{\text{ labelled by }\vecx^\vece}
		\times 
		\underbrace{\begin{bmatrix}
			& & \text{ } & \text{ } & \text{ } & &\\
			& & \text{ } & \text{ } & \text{ } & &\\
			& & \text{ } & \text{ } & \text{ } & &\\
			& & \text{ } & M_{\phi} & \text{ } & &\\ 
			& & \text{ } & \text{ } & \text{ } & &\\
			& & \text{ } & \text{ } & \text{ } & &\\
			& & \text{ } & \text{ } & \text{ } &
		\end{bmatrix}}_{\text{labelled by } \vecy^\vecd}
	\]\pause
	where
	\[
		M_\phi(\vecx^\vece, \vecy^\vecd) = \left\{
		\begin{array}{ll}
			\coeff_{\vecy^\vecd}(\phi(\vecx^\vece)) & \mbox{if } \sum e_i = \sum d_i \\
			0 & \mbox{ otherwise} 
		\end{array}
		\right.
	\]
\end{frame}

\begin{frame}{What makes Vandermonde type matrices work?}
	\only<1-2>{\[\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	\times 
	\begin{bmatrix}
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & M & \text{ }\\ 
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }\\
	\text{ } & \text{ } & \text{ }
	\end{bmatrix}
	=
	\begin{bmatrix}
	& \text{ } &\\
	& AM &\\ 
	& \text{ } &
	\end{bmatrix}\]
	
	\vspace{3em}}
	
	\only<2->{ 
		\begin{center}
			\textbf{Cauchy-Binet}: $\det(AM) = \sum_{B \subseteq \set{x_i} \text{, } \abs{B} = k} \det(A_B) \det(M_B).$
		\end{center}}
	
	\only<3>{\[\begin{bmatrix}
		s & s^2 & \ldots & s^k\\
		s^2 & s^4 & \ldots & s^{2k}\\
		\vdots & \vdots & \text{ } & \vdots\\
		\vdots & \vdots & \ddots & \vdots\\
		\vdots & \vdots & \ddots & \vdots\\
		\vdots & \vdots & \text{ } & \vdots\\
		s^n & s^{2n} & \ldots & s^{kn}
		\end{bmatrix}\]}
	\only<4>{\[\begin{bmatrix}
		s & \ldots & s^k\\
		\inparen{s^2}^1& \ldots & \inparen{s^2}^k\\
		\vdots & \text{ } & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \text{ } & \vdots\\
		\inparen{s^n}^1 & \ldots & \inparen{s^n}^k
		\end{bmatrix}\]}
	\only<5->{\[\begin{array}{r}
		x_1\\
		x_2\\
		\vdots\\
		\vdots\\
		\vdots\\
		\vdots\\
		x_n
		\end{array}
		\begin{bmatrix}
		\inparen{s^{\wt(x_1)}}^1 & \ldots & (s^{\wt(x_1)})^k\\
		\inparen{s^{\wt(x_2)}}^1& \ldots & \inparen{s^{\wt(x_2)}}^k\\
		\vdots & \text{ } & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \ddots & \vdots\\
		\vdots & \text{ } & \vdots\\
		\inparen{s^{\wt(x_n)}}^1 & \ldots & \inparen{s^{\wt(x_n)}}^k
		\end{bmatrix} 
		\quad \quad 
		\only<5-8>{\wt(x_i) = i}
		\only<9->{\wt(x_i) \text{ is distinct for each } i}\]}
	
	\only<6->{\begin{itemize}
			\item If $B = \inparen{x_{i_1}, x_{i_2}, \ldots, x_{i_k}}$, then $\wt(B) = \sum_{j=1}^{k} j \wt(x_{i_j})$ \pause
			\uncover<7->{\item $\deg_s(\det(M_B)) = \wt(B)$} \pause
			\uncover<8->{\item Isolate a unique non-zero minor of $A$ with maximum weight}
			\end{itemize}}	
\end{frame}

\begin{frame}{The Current Matrix}
	\begin{tikzpicture}
	\draw[thick] (0cm,0cm) rectangle (4cm,6cm); 
	\node(rows) at (-.5cm, 3cm) {$\vecx^\vece$};
	\draw[->, thick] ($(rows)+(0cm,.5cm)$) -- (-.5cm,6cm);
	\draw[->, thick] ($(rows)+(0cm,-.5cm)$) -- (-.5cm,0cm);
	\node(cols) at (2cm, -.5cm) {$\vecy^\vecd$};
	\draw[->, thick] ($(cols)+(.5cm,0cm)$) -- (4cm,-.5cm);
	\draw[->, thick] ($(cols)+(-.5cm,0cm)$) -- (0cm,-.5cm);\pause
	\draw[thick] (.5cm,5.25cm) rectangle (0cm,6cm);
	\node(one) at (.25cm, 5.625cm) {$1$};
	\draw[thick] (.5cm,5.25cm) rectangle (1.25cm,4.25cm);
	\node(two) at (.825cm, 4.75cm) {$2$};
	\node(ddots1) at (1.5cm, 3.75cm) {$\ddots$};
	\node(ddots2) at (2cm, 3cm) {$\ddots$};
	\draw[thick] (2.5cm,2.5cm) rectangle (4cm,0cm);
	\node(tee) at (3.25cm, 1.25cm) {$t$};\pause
	\node(entry) at (7.5cm, 5.5cm) {$M_\phi(\vecx^\vece, \vecy^\vecd) = \coeff_{\vecy^\vecd}(\phi(\vecx^\vece))$};\pause
	\node at (7.5cm, 4cm) {\textbf{Taking inspiration from the}};
	\node at (7.5cm, 3.5cm) {\textbf{prev. case}: $M_\phi(x_i, y_j) = s^{\wt(i)j}$}; \pause
	\node at (7.75cm, 2.75cm) {$\wt(\vecx^\vece) = \sum_{i \in [n]} e_i \wt(i)$};
	\pause
	\node at (7.75cm, 2cm) {$M_\phi(\vecx^\vece, y_j^d) = s^{\wt(\vecx^\vece)j}$};
	\pause
	\node at (7.75cm, .75cm) {If $B = \inparen{\vecx^{\vece_1}, \vecx^{\vece_2}, \ldots, \vecx^{\vece_k}}$,};
	\node at (7.75cm, 0cm) {then $\wt(B) = \sum_{j \in [k]} j \wt(\vecx^{\vece_j})$};
	\end{tikzpicture}
\end{frame}

\begin{frame}{A Rank Preserving Matrix}
	\[\uncover<2->{\begin{array}{r}
	\uparrow\\
	k\\
	\downarrow\\
	\end{array}}
	\begin{bmatrix}
	& \text{ } & \tmark{a}\text{ } & \text{ } & \text{ } & \text{ } &\\ 
	& \text{ } & \text{ } & A & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } & \text{ }\tmark{b} & \text{ } &
	\end{bmatrix}
	\times 
	\only<1-4>{\begin{bmatrix}
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & M & \text{ } &\\ 
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &\\
	& \text{ } & \text{ } & \text{ } &
	\end{bmatrix}
	=
	\begin{bmatrix}
	\text{ } & \text{ } & \text{ }\\
	\text{ } & AM & \text{ }\\ 
	\text{ } & \text{ } & \text{ }
	\end{bmatrix}}
	\only<5->{\begin{bmatrix}
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & M' & \text{ }\\ 
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }\\
		\text{ } & \text{ } & \text{ }
		\end{bmatrix}
		=
		\begin{bmatrix}
		& \text{ } &\\
		& AM' &\\ 
		& \text{ } &
		\end{bmatrix}}
	\]\pause
	\begin{tikzpicture}[remember picture, overlay]
	\only<2-4>{\node (t) at ($(6.325cm, -.125cm)$) {$>k$};
	\draw[->, thick] ($(5.825cm, -.125cm)$) -- ($(5.125cm, -.125cm)$);
	\draw[->, thick] ($(6.825cm, -.125cm)$) -- ($(7.5cm, -.125cm)$);}
	\only<5->{\node (t) at ($(6.325cm, -.125cm)$) {$k$};
		\draw[->, thick] ($(6.125cm, -.125cm)$) -- ($(5.5cm, -.125cm)$);
		\draw[->, thick] ($(6.5cm, -.125cm)$) -- ($(7.325cm, -.125cm)$);}
	\only<4->{\draw[blue, thick] ($(a)+(.05cm,.1cm)$) rectangle ($(b)-(.05cm,0cm)$);} 
	\end{tikzpicture}\pause
	
	\vspace{2em}
	\textbf{What we want}: $k$ columns of $AM$ that are linearly independent.\\ \pause
	\vspace{1em}
	\textbf{Proof Strategy}: 
	\begin{itemize}
		\item Isolate a unique non-zero minor $A_{B_0}$ with maximum weight \pause
		\item $M' \equiv$ $k$ columns of $M$ such that $\deg_s(\det(M'_{B_0})) = \wt(B_0)$
	\end{itemize}
\end{frame}

\begin{frame}{A few details}
	\textbf{About $\deg_s(\det(M'_{B_0}))$ for $B \neq B_0$}:\\
	\begin{itemize}
		\item $\deg_s(\det(M'_B)) \leq \wt(B)$ for $B \neq B_0$ \pause
	\end{itemize}
	\vspace{1em}
	\textbf{About $\wt$}:\\
	\begin{itemize}
		\item $\wt$ "hashes" the monomials in question\\ $\Rightarrow$ there is a unique $B$ of maximum weight. \pause
	\end{itemize}
	\vspace{1em}
	\textbf{About $M'$}
	\begin{itemize}
		\item $M'$ can always be chosen such that its columns are indexed by "pure" monomials.
	\end{itemize}
\end{frame}

\begin{frame}{A Faithful Map over Arbitrary Fields}
\vspace{-1.5em}
	\[\phi : x_i \to \sum_{j=1}^{k}s^{\wt(i)j} y_j + a_iy_0 \text{ and } z_i \to \sum_{j=1}^{k}s^{\wt(i)j} w_j + a_iy_0\] where $t$ is the inseparable degree and $\wt(i) = (t+1)^i \bmod p$.\pause \\
	\vspace{1em}
	\underline{\textbf{Properties}}
	\begin{enumerate}
		\vspace{.5em}
		\item For every $\vecu$, there is a $\vecv$ for which $\Ech(\vecf(\phi), \vecu) = \Ech(\vecf(\phi), \vecv(\phi))$\\ \pause
		\vspace{.5em}
		\item $\phi(\Ech(\vecf,\vecv)) \times M_\phi$ is a sub-matrix of $\Ech(\vecf(\phi), \vecv(\phi))$\\ \pause
		\vspace{.5em}
		\item $\rank(\phi(\Ech(\vecf,\vecv))) = \rank(\phi(\Ech(\vecf,\vecv)) \times M_\phi)$\\ \pause
		\vspace{.5em}
		\item $\rank(\Ech(\vecf, \vecv)) = \rank(\phi(\Ech(\vecf,\vecv)))$
	\end{enumerate}
\end{frame}

\begin{frame}{An Instantiation}
	\begin{theorem}
		Let $\fm \in \F[\x]$ be $s$-sparse polynomials such that $\algrank(\fm) = k$ and the inseparable degree is $t$.\\
		If $t$ and $k$ are bounded by a constant, then, there is an explicit deterministic construction of a faithful homomorphisms in $\poly(n,m,s)$ time.
	\end{theorem}\pause
	\vspace{2em}
	Explicit faithful homomorphisms can also be constructed efficiently for other models studied in [ASSS16] when we have similar inseparable degree bounds.
\end{frame}

\begin{frame}{Open Threads}
	\begin{enumerate}
		\item Improve the dependence on "inseparable degree".\\ \pause
		\vspace{2em}
		\item [GSS18]: Different characterisation for Algebraic dependence - not algorithmic but has no dependence on "inseparable degree"\\
		\vspace{1em}
		Can we get PIT applications out of it?\pause
	\end{enumerate}
	\vspace{3em}
	\begin{center}
		\textbf{\large{Thank you!}}
	\end{center}
\end{frame}

\nocite{*}
\bibliographystyle{alpha}
\bibliography{bib.bib}
\end{document}